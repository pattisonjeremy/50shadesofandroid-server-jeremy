# Time Bomb Server

This repository holds all the deploy binaries for the server.

## Build Instructions

### Building

The server is deployed through Docker.

To build the server executable, open `File > Build Settings` and select `PC, Mac & Linux Standalone`.

Ensure the following settings are used:

    Target Platform is Linux
    Architecture    is x86_64
    Headless Mode   is ticked

Save the executable in the `bin` folder, overwriting the existing binary. Add and commit the binary to the repository as usual.

### Deploying

A Nectar server has been set up with a post-receive hook to automatically deploy any branch that is pushed to it.

    git remote add server ssh://ubuntu@vm-115-146-89-128.melbourne.rc.nectar.org.au/~/timebomb.git
    git push server master

You may need to request SSH access to the Nectar server to push.

Alternatively, to run the server locally with Docker, run the following command in the project directory. You need docker-compose installed as well as Docker.

    docker-compose up --build

## Multiplayer

The multiplayer server is currently hosted at `vm-115-146-89-128.melbourne.rc.nectar.org.au`. The Dockerfile exposes port 7777 as the game port.
