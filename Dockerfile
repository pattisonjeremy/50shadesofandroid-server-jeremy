FROM ubuntu:16.04

RUN apt-get update && apt-get install -y xvfb libxcursor1 libxrandr2

WORKDIR /app
ADD . .

EXPOSE 7777